package controller;

import model.logic.TaxiTripsManager;


public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static TaxiTripsManager  manager = new TaxiTripsManager();

	/** To load the services of the taxi with taxiId */
	public static void loadServices( String serviceFile, double pDx) {
		manager.loadServices(serviceFile,pDx);
	}
}
