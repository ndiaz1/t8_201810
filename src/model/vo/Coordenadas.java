package model.vo;

public class Coordenadas implements Comparable<Coordenadas> {
	private double latitud,longitud;
	
	public Coordenadas(double pLat, double pLong){
		latitud = pLat;
		longitud = pLong;
	}
	
	public double getLatitude(){
		return latitud;
	}
	public double getLongitude(){
		return longitud;
	}
	public void setLatitude(double pLatitud){
		latitud = pLatitud;
	}
	public void setLongitude(double pLongitud){
		longitud = pLongitud;
	}
	public int compareTo(Coordenadas pCoor){
		int rta = 0;
		if(latitud > pCoor.getLatitude() && longitud > pCoor.getLongitude()){
			rta = 1;
		}
		else if(latitud < pCoor.getLatitude() && longitud < pCoor.getLongitude()){
			rta = -1;
		}
		return rta;
	}
}
