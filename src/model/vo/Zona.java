package model.vo;

import model.data_structures.*;

public class Zona {

	public double latReferencia;
	public double lonReferencia;
	public Coordenadas referencia;
	public double Dx;
	private ListKV<String,Service> pickupServiciosZona;
	private ListKV<String,Service> dropoffServiciosZona;

	public Zona(double pLatitud, double pLongitud, double pDx){
		latReferencia = pLatitud;
		lonReferencia = pLongitud;
		referencia = new Coordenadas(pLatitud, pLongitud);
		Dx = pDx;
		pickupServiciosZona = new ListKV<String, Service>();
		dropoffServiciosZona = new ListKV<String, Service>();
	}

	public double getLatRef(){return latReferencia;}
	public double getLonRef(){return lonReferencia;}
	public Coordenadas darReferencia(){return referencia;}
	public double darDx(){return Dx;}
	public ListKV<String,Service> darServiciosZonaPickup(){return pickupServiciosZona;}
	public ListKV<String,Service> darServiciosZonaDropoff(){return dropoffServiciosZona;}

	public double toRad(double pAngle){
		return (Math.PI/180)*pAngle;
	}
	public double distanciaHarvesiana(double lat2, double lon2){
		final int R = 6371*1000;
		double latDistance = toRad(lat2-latReferencia);
		double lonDistance = toRad(lon2-lonReferencia);
		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(toRad(latReferencia))* Math.cos(toRad(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c; //en metros
		double resultado = distance/1609.34; // en millas
		return resultado;
	}
	public boolean pickupDentroDelRango(Service servicio){
		boolean dentro = false;
		double distanciaARef = distanciaHarvesiana(servicio.getPickupLat(), servicio.getPickupLong());
		if(distanciaARef <= Dx){
			dentro = true;
		}
		return dentro;
	}

	public boolean dropoffDentroDelRango(Service servicio){
		boolean dentro = false;
		double distanciaARef = distanciaHarvesiana(servicio.getDropoffLat(), servicio.getDropoffLong());
		if(distanciaARef <= Dx){
			dentro = true;
		}
		return dentro;
	}

	public void agregarServicioPickup(Service pServicio){
		if(pickupDentroDelRango(pServicio)==true){
			pickupServiciosZona.put(pServicio.getTripId(), pServicio);
		}
	}

	public void agregarServicioDropoff(Service pServicio){
		if(dropoffDentroDelRango(pServicio)==true){
			dropoffServiciosZona.put(pServicio.getTripId(), pServicio);
		}
	}
	public double darDistanciaPromedioP(){
		double distanciaProm = 0;
		int numeroTotal = 0;
		NodoKV<String, Service> actual = pickupServiciosZona.darPrimero();
		while(actual != null){
			Service sActual = actual.getValue();
			distanciaProm+=sActual.getTripMiles();
			numeroTotal++;
			actual = actual.darSiguiente();
		}
		return distanciaProm/numeroTotal;
	}
	public double darValorPromedioP(){
		double valorProm = 0;
		int numeroTotal = 0;
		NodoKV<String, Service> actual = pickupServiciosZona.darPrimero();
		while(actual != null){
			Service sActual = actual.getValue();
			valorProm+=sActual.getTripTotal();
			numeroTotal++;
			actual = actual.darSiguiente();
		}
		return valorProm/numeroTotal;
	}
	public double darTiempoPromedioP(){
		double tiempoProm = 0;
		int numeroTotal = 0;
		NodoKV<String, Service> actual = pickupServiciosZona.darPrimero();
		while(actual != null){
			Service sActual = actual.getValue();
			tiempoProm+=sActual.getTripSeconds();
			numeroTotal++;
			actual = actual.darSiguiente();
		}
		return tiempoProm/numeroTotal;
	}
	public double darNumeroPeajesP(){
		double peajes = 0;
		NodoKV<String, Service> actual = pickupServiciosZona.darPrimero();
		while(actual != null){
			Service sActual = actual.getValue();
			if(sActual.getTolls() != 0){
				peajes++;
			}
			actual = actual.darSiguiente();
		}
		return peajes;
	}
	public double darDistanciaPromedioD(){
		double distanciaProm = 0;
		int numeroTotal = 0;
		NodoKV<String, Service> actual2 = dropoffServiciosZona.darPrimero();
		while(actual2 != null){
			Service sActual2 = actual2.getValue();
			distanciaProm+=sActual2.getTripMiles();
			numeroTotal++;
			actual2 = actual2.darSiguiente();
		}
		return distanciaProm/numeroTotal;
	}
	public double darValorPromedioD(){
		double valorProm = 0;
		int numeroTotal = 0;
		NodoKV<String, Service> actual2 = dropoffServiciosZona.darPrimero();
		while(actual2 != null){
			Service sActual2 = actual2.getValue();
			valorProm+=sActual2.getTripTotal();
			numeroTotal++;
			actual2 = actual2.darSiguiente();
		}
		return valorProm/numeroTotal;
	}
	public double darTiempoPromedioD(){
		double tiempoProm = 0;
		int numeroTotal = 0;
		NodoKV<String, Service> actual2 = dropoffServiciosZona.darPrimero();
		while(actual2 != null){
			Service sActual2 = actual2.getValue();
			tiempoProm+=sActual2.getTripSeconds();
			numeroTotal++;
			actual2 = actual2.darSiguiente();
		}
		return tiempoProm/numeroTotal;
	}
	public double darNumeroPeajesD(){
		double peajes = 0;
		NodoKV<String, Service> actual2 = dropoffServiciosZona.darPrimero();
		while(actual2!= null){
			Service sActual2 = actual2.getValue();
			if(sActual2.getTolls() != 0){
				peajes++;
			}
			actual2= actual2.darSiguiente();
		}
		return peajes;
	}
	
	public double aportePesoPickup(){return darDistanciaPromedioP()+darValorPromedioP()+darTiempoPromedioP()+darNumeroPeajesP();}
	public double aportePesoDropoff(){return darDistanciaPromedioD()+darValorPromedioD()+darTiempoPromedioD()+darNumeroPeajesD();}
}
