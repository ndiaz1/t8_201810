package model.data_structures;

public class Edge<Key extends Comparable <Key>, Value,A> {
	
	private Vertex<Key,Value,A> origen;
	private Vertex<Key,Value,A> destino;
	private A peso;
	
	public Edge(Vertex<Key,Value,A> pOrigen, Vertex<Key,Value,A> pDestino, A pPeso){
		origen = pOrigen;
		destino = pDestino;
		peso = pPeso;
	}
	
	public Vertex<Key,Value,A> getOrigen(){return origen;}
	public Vertex<Key,Value,A> getDestino(){return destino;}
	public A getPeso(){return peso;}
	
	public void cambiarOrigen(Vertex<Key,Value,A> nOrigen){
		origen = nOrigen;
	}
	public void cambiarDestino(Vertex<Key,Value,A> nDestino){
		destino = nDestino;
	}
	public void cambiarPeso(A nPeso){
		peso = nPeso;
	}

}
