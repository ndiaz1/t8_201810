package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<T> implements Iterable<T>{

	/**
	 * Primer nodo de la cola.
	 */
	private Nodo<T> primero;
	
	/**
	 * Ultimo nodo de la cola.
	 */
	private Nodo<T> ultimo;
	
	/**
	 * Longitud de la cola.
	 */
	private int longitud;
	
	/**
	 * Crea una nueva cola.
	 */
	public Queue(){
		primero = null;
		ultimo = null;
		longitud = 0;
	}
	
	/**
	 * Retorna el primer nodo de la cola.
	 * @return primer nodo de la cola.
	 */
	public Nodo<T> darPrimero(){
		return primero;
	}
	
	/**
	 * Retorna el �ltimo nodo de la cola.
	 * @return �ltimo nodo de la cola.
	 */
	public Nodo<T> darUltimo(){
		return ultimo;
	}
	
	/**
	 * Agrega un nuevo elemento a la cola.
	 */
	public void enqueue(T elemento) {
		Nodo<T> nuevo = new Nodo<T>(elemento);
		if(!isEmpty()){
			ultimo.cambiarSiguiente(nuevo);
			ultimo = nuevo;
			longitud++;
		}
		else{
			primero = nuevo;
			ultimo = primero;
			longitud++;
		}
	}

	/**
	 * Elimina el primer elemento de la cola y lo retorna. Si la cola est� vac�a, retorna null.
	 */
	public T dequeue() {
		if(!isEmpty()){
		Nodo<T> dequeued = primero;
		primero = primero.darSiguiente();
		longitud--;
		return dequeued.darElemento();
		}
		else{
			return null;
		}
	}

	/**
	 * Retorna true si la cola est� vac�a (no tiene ning�n elemento). Retorna false de lo contrario.
	 */
	public boolean isEmpty() {
		return primero==null;
	}
	
	public T peek() {
        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        return primero.darElemento();
    }
	
    public Iterator<T> iterator()  {
        return new ListIterator<T>(primero);  
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ListIterator<T> implements Iterator<T> {
        private Nodo<T> current;

        public ListIterator(Nodo<T> first) {
            current = first;
        }

        public boolean hasNext()  { return current != null;                     }
        public void remove()      { throw new UnsupportedOperationException();  }

        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            T item = current.darElemento();
            current = current.darSiguiente(); 
            return item;
        }
    }

	/**
	 * Retorna el tama�o de la cola.
	 */
	public int size() {
		return longitud;
	}

}
