package model.data_structures;

public class DiGraph<Key extends Comparable<Key>,Value,A> {
	
	private int V; //vertices
	private int E; //arcos
	private ListKV<Key,Vertex<Key,Value,A>> adj;
	private List<Edge<Key,Value,A>> arcos;
	
	public DiGraph(){
		V = 0;
		E = 0;
		adj = new ListKV<Key,Vertex<Key,Value,A>>();
		arcos = new List<Edge<Key,Value,A>>();
	}
	public int V(){return V;}
	public int E(){return E;}
	public ListKV<Key,Vertex<Key,Value,A>> darListaAdyacencia(){return adj;}
	public List<Edge<Key,Value,A>> darArcos(){return arcos;}
	
	//Agrega un vertice nuevo (Sin conexiones a otros vertices). Si el vertice existe, no hace nada.
	public void addVertex(Key idVertex, Value infoVertex){
		Vertex<Key,Value,A> nuevo = new Vertex<Key,Value,A>(idVertex, infoVertex);
		if(!adj.contains(idVertex)){
			adj.put(idVertex, nuevo);
			V++;
		}
	}
	
	//Agrega el arco si los dos vertices ya existen en el grafo y no estan conectados. infoArc corresponde al peso del arco.
	public void addEdge(Key idVertexIni, Key idVertexFin, A infoArc){
		Vertex<Key,Value,A> v1 = adj.get(idVertexIni);
		Vertex<Key,Value,A> v2 = adj.get(idVertexFin);
		if(v1 != null && v2!= null && v1.estaConectadoA(v2) == false){
			Edge<Key,Value,A> nuevo = new Edge<Key,Value,A>(v1, v2, infoArc);
			arcos.agregar(nuevo);
			v1.agregarDestino(v2, infoArc);
			E++;
		}
	}
	
	public Value getInfoVertex(Key idVertex){
		Vertex<Key, Value, A> buscado = adj.get(idVertex);
		if(buscado != null){return buscado.getValue();}
		else{return null;}
	}
	
	public void setInfoVertex(Key idVertex, Value infoVertex){
		Vertex<Key, Value, A> buscado = adj.get(idVertex);
		if(buscado != null){buscado.setInfoVertex(infoVertex);}
	}
	
	public A getInfoArc(Key idVertexIni, Key idVertexFin){
		Vertex<Key, Value, A> v1 = adj.get(idVertexIni);
		Vertex<Key, Value, A> v2 = adj.get(idVertexFin);
		Edge<Key,Value,A> buscado = null;
		if(v1 != null && v2 != null){
			buscado = v1.buscarArco(v2);
			return buscado.getPeso();
		}
		else{
			return null;
		}
	}
	public void setInfoArc(Key idVertexIni, Key idVertexFin, A infoArc){
		Vertex<Key, Value, A> v1 = adj.get(idVertexIni);
		Vertex<Key, Value, A> v2 = adj.get(idVertexFin);
		Edge<Key,Value,A> buscado = null;
		if(v1 != null && v2 != null && v1.estaConectadoA(v2)){
			buscado = v1.buscarArco(v2);
			buscado.cambiarPeso(infoArc);
		}
	}
	Iterable<Edge<Key,Value,A>>adj(Key idVertex){
		Vertex<Key,Value,A> vert = adj.get(idVertex);
		return vert.getAdyacentes().iterator();
	}
}
