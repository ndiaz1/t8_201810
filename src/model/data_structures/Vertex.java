package model.data_structures;

public class Vertex<Key extends Comparable<Key>,Value,A>{
	
	private Key key;
	private Value value;
	private List<Edge<Key,Value,A>> adjacent;
	
	public Vertex(Key pKey, Value pValue){
		key = pKey;
		value = pValue;
		adjacent = new List<Edge<Key,Value,A>>();
	}
	
	public Key getKey(){return key;}
	public Value getValue(){return value;}
	public List<Edge<Key,Value,A>> getAdyacentes(){return adjacent;}
	
	//Supone this como Origen y pAdyacente como Destino
	public void agregarDestino(Vertex<Key,Value,A> pDestino, A pPeso){
		Edge<Key,Value,A> nuevo = new Edge<Key,Value,A>(this, pDestino, pPeso);
		adjacent.agregar(nuevo);
	}
	
	public void setInfoVertex(Value pInfo){
		value = pInfo;
	}
	
	//retorna el arco asociado a this y al vertice pasado por parametro. Null si los vertices no estan conectados
	public Edge<Key,Value,A> buscarArco(Vertex<Key,Value,A> pDestino){
		Nodo<Edge<Key,Value,A>> actual = getAdyacentes().darPrimero();
		Edge<Key,Value,A> buscado = null;
		while(actual != null){
			Edge<Key,Value,A> eActual = actual.darElemento();
			if(eActual.getDestino().equals(pDestino)){
				buscado = eActual;
			}
			actual = actual.darSiguiente();
		}
		return buscado;
	}
	public boolean estaConectadoA(Vertex<Key,Value,A> pVertex){
		return buscarArco(pVertex)!= null;
	}
}
