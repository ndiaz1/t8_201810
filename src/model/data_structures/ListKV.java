package model.data_structures;

public class ListKV<Key extends Comparable<Key>, Value>{
	
	/**
	 * Primer nodo de la lista 
	 */
	private NodoKV<Key,Value> primero;
	
	/**
	 * Longitud de la lista.
	 */
	private int longitud;
	
	/**
	 * Crea una nueva lista sin elementos.
	 */
	public ListKV(){
		primero = null;
		longitud = 0;
	}
	
	/**
	 * Retorna el primer nodo de la lista.
	 * @return primer nodo de la lista.
	 */
	public NodoKV<Key,Value> darPrimero(){
		return primero;
	}
	
	/**
	 * Retorna la longitud de la lista.
	 * @return Longitud de la lista.
	 */
	public int darLongitud()
	{
		return longitud;
	}
	
	public boolean isEmpty() {
        return darLongitud() == 0;
    }
	
	public Value get(Key key) {
        for (NodoKV<Key,Value> x = primero; x != null; x = x.darSiguiente()) {
            if (key.compareTo(x.getKey())==0)
                return x.getValue();
        }
        return null;
    }
	
	public boolean contains(Key key) {
        return get(key) != null;
    }
	
	public void put(Key key, Value val) {
        if(val != null){
        	for(NodoKV<Key,Value> actual = primero; actual!=null; actual = actual.darSiguiente()){
        		if(key.compareTo(actual.getKey())==0){
        			actual.cambiarValor(val);
        			return;
        		}
        	}
        	primero = new NodoKV<Key,Value>(key, val, primero);
        	longitud++;
        }
    }
	
	public NodoKV<Key,Value> delete(Key key){
		NodoKV<Key,Value> borrado = null;
		if(primero.getKey().compareTo(key)==0){
			borrado = primero;
			primero = primero.darSiguiente();
			longitud--;
		}
		else{
			NodoKV<Key,Value> actual = primero;
			while(actual.darSiguiente()!=null){
				if(actual.darSiguiente().getKey().compareTo(key)==0){
					borrado = actual.darSiguiente();
					actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
				}
				else{
					actual = actual.darSiguiente();
				}
			}
			if(borrado == null && actual.getKey().compareTo(key)==0){
				borrado = actual;
				actual = null;
			}
		}
		return borrado;
	}
	
	public Iterable<Key> keys()  {
        Queue<Key> queue = new Queue<Key>();
        for (NodoKV<Key, Value> x = primero; x != null; x = x.darSiguiente())
            queue.enqueue(x.getKey());
        return queue;
    }
}