package model.data_structures;

public class NodoKV<Key extends Comparable<Key>,Value>{
	
	private Key key;
	private Value value;
	private NodoKV<Key,Value> siguiente;
	
	public NodoKV(Key pLlave, Value pValor, NodoKV<Key,Value> pSiguiente){
		key = pLlave;
		value = pValor;
		siguiente = pSiguiente;
	}

	public Key getKey(){
		return key;
	}
	
	public Value getValue(){
		return value;
	}
	
	public NodoKV<Key,Value> darSiguiente(){
		return siguiente;
	}
	
	public void cambiarSiguiente(NodoKV<Key,Value> pNuevo){
		siguiente = pNuevo;
	}
	
	public void cambiarValor(Value pNuevo){
		value = pNuevo;
	}
}
