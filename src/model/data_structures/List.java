package model.data_structures;

public class List<T>{

	/**
	 * Primer nodo de la lista 
	 */
	public Nodo<T> primero;

	/**
	 * Ultimo nodo de la lista.
	 */
	public Nodo<T> ultimo;

	/**
	 * Longitud de la lista.
	 */
	public int longitud;

	/**
	 * Crea una nueva lista sin elementos.
	 */
	public List(){
		primero = null;
		ultimo = null;
		longitud = 0;
	}

	/**
	 * Retorna el primer nodo de la lista.
	 * @return primer nodo de la lista.
	 */
	public Nodo<T> darPrimero(){
		return primero;
	}

	/**
	 * Retorna el �ltimo nodo de la lista
	 * @return �ltimo nodo de la lista.
	 */
	public Nodo<T> darUltimo(){
		return ultimo;
	}

	/**
	 * Retorna la longitud de la lista.
	 * @return Longitud de la lista.
	 */
	public int darLongitud()
	{
		return longitud;
	}

	/**
	 * Agrega un nuevo nodo a la lista con el elemento dado por par�metro.
	 */
	public boolean agregar(T n)
	{
		boolean agrego = false;
		Nodo<T> nuevo = new Nodo<T>(n);
		if(primero == null)
		{
			primero = nuevo;
			ultimo = nuevo;
			longitud++;
			agrego = true;
		}
		else
		{
			ultimo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ultimo);
			ultimo = nuevo;
			longitud ++;
			agrego = true;
		}
		return agrego;
	}
	public void cambiarPrimero(Nodo<T> p) {
		primero=p;
	}

	/**
	 * Elimina el nodo con el elemento dado por par�metro.
	 */
	public boolean eliminar(T e)
	{
		boolean elim = false;
		Nodo<T> aElim = buscar(e);
		if(aElim != null){
			aElim.darSiguiente().cambiarAnterior(aElim.darAnterior());
			aElim.darAnterior().cambiarSiguiente(aElim.darSiguiente());
			elim = true;
		}
		return elim;
	}

	public boolean isEmpty(){
		return longitud==0;
	}

	public Nodo<T> buscar(T pElem){
		Nodo<T> actual = this.darPrimero();
		while(actual!=null){
			if(actual.darElemento().equals(pElem)){
				return actual;
			}
			actual = actual.darSiguiente();
		}
		return null;
	}
	
	public Iterable<T> iterator()  {
        Queue<T> queue = new Queue<T>();
        for (Nodo<T> x = primero; x != null; x = x.darSiguiente())
            queue.enqueue(x.darElemento());
        return queue;
    }
}