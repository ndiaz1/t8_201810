package model.data_structures;

//Adaptado de http://homepage.divms.uiowa.edu/~kvaradar/sp2009/SeparateChainingHashTable.java

public class HashTable<Key extends Comparable <Key>, Value> {

	private int capacidad;
	private int tamanoActual;
	private ListKV<Key,Value>[] areaPrimaria;

	public HashTable(int pCapacidad){
		areaPrimaria = new ListKV[nextPrime(pCapacidad)];
		for(int i = 0; i<areaPrimaria.length;i++){
			areaPrimaria[i]=new ListKV<Key,Value>();
		}
		capacidad = pCapacidad;
		tamanoActual = 0;
	}
	
	public void calcularTamano(){
		int tamano = 0;
		for(int i = 0; i<areaPrimaria.length;i++){
			tamano+=areaPrimaria[i].darLongitud();
		}
		tamanoActual = tamano;
	}
	
	public int getTamano(){
		calcularTamano();
		return tamanoActual;
	}
	
	public int getCapacidad(){
		return capacidad;
	}
	
	public boolean isEmpty(){
		return getTamano() == 0;
	}
	
	public boolean contains(Key key){
		return get(key)!=null;
	}
	

	/**
	 * Agrega una nueva dupla en el arreglo. Si la llave es null, no altera el arreglo y no agrega la dupla. Si el valor es null, elimina la dupla.
	 * @param key Llave a ingresar
	 * @param value Valor de la dupla
	 */
	public void put(Key key, Value value){
		if(key!=null){
			int posicion = hash(key);
			if(value == null){
				areaPrimaria[posicion].delete(key);
			}
			else{
				areaPrimaria[posicion].put(key, value);
				rehash();
			}
		}
	}
	
	/**
	 * @param key Llave de la dupla.
	 * @return Valor correspondiente a la llave. Null si la llave no existe.
	 */
	public Value get(Key key){
		if(key != null){
			Value buscado = areaPrimaria[hash(key)].get(key);
			return buscado;
		}
		else{
			return null;
		}
	}
	
	/**
	 * Elimina el nodo de acuerdo a la llave dada por parámetro.
	 * @param key Llave del nodo a eliminar.
	 * @return Valor eliminado.
	 */
	public void delete(Key key){
		if(key != null)
		{
			if(areaPrimaria[hash(key)].contains(key)){
				areaPrimaria[hash(key)].delete(key);
			}
			calcularTamano();
		}
	}

	public void rehash(){
		calcularTamano();
		if(++tamanoActual>capacidad){
			ListKV<Key,Value>[] listaAntigua = areaPrimaria;
			areaPrimaria = new ListKV[nextPrime(capacidad*2)];
			for(int i = 0; i<areaPrimaria.length;i++){
				areaPrimaria[i]=new ListKV<Key,Value>();
			}
			tamanoActual = 0;
			capacidad = 2*capacidad;
			for( int i = 0; i < listaAntigua.length; i++ ){
				NodoKV<Key, Value> actual = listaAntigua[i].darPrimero();
				while(actual != null){
					Key kActual = actual.getKey();
					Value vActual = actual.getValue();
					put(kActual,vActual);
					actual = actual.darSiguiente();
				}
			}
		}
	}

	private int hash(Key key){
		return (key.hashCode() & 0x7fffffff) % capacidad;
	}
	
	public Iterable<Key> keys() {
        Queue<Key> queue = new Queue<Key>();
        for (int i = 0; i < tamanoActual; i++) {
            for (Key key : areaPrimaria[i].keys())
                queue.enqueue(key);
        }
        return queue;
    } 

	/**
	 * Internal method to find a prime number at least as large as n.
	 * @param n the starting number (must be positive).
	 * @return a prime number larger than or equal to n.
	 */
	private static int nextPrime( int n )
	{
		if( n % 2 == 0 )
			n++;

		for( ; !isPrime( n ); n += 2 )
			;

		return n;
	}

	/**
	 * Internal method to test if a number is prime.
	 * Not an efficient algorithm.
	 * @param n the number to test.
	 * @return the result of the test.
	 */
	private static boolean isPrime( int n )
	{
		if( n == 2 || n == 3 )
			return true;

		if( n == 1 || n % 2 == 0 )
			return false;

		for( int i = 3; i * i <= n; i += 2 )
			if( n % i == 0 )
				return false;

		return true;
	}
}
