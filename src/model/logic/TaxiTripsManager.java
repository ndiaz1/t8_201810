package model.logic;


import model.data_structures.*;
import model.vo.*;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class TaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	
	private DiGraph<Coordenadas,Zona,Double> grafo;
	
	public void loadServices(String direccionJson, double pDx) {
		grafo = new DiGraph<Coordenadas, Zona, Double>();
		System.out.println("Cargando datos de  " + direccionJson);
		System.out.println("\n-------------------------------------------------------");
		JsonParser parser = new JsonParser();
		try{
			JsonArray arr = (JsonArray) parser.parse(new FileReader(direccionJson));
			for (int i = 0; arr != null && i < arr.size(); i++){
				JsonObject obj= (JsonObject) arr.get(i);

				String id = "Unknown";
				if ( obj.get("taxi_id") != null ){
					id = obj.get("taxi_id").getAsString();}

				String comp = "Independent Owner";
				if ( obj.get("company") != null ){ 
					comp = obj.get("company").getAsString();}

				String dca = "Unknown";
				if ( obj.get("dropoff_community_area") != null ){
					dca = obj.get("dropoff_community_area").getAsString(); }

				String pca = "Unknown";
				if ( obj.get("pickup_community_area") != null ){
					pca = obj.get("pickup_community_area").getAsString(); }

				String tripid = "Unknown";
				if ( obj.get("trip_id") != null ){
					tripid = obj.get("trip_id").getAsString(); }

				String tripst = "Unknown";
				if ( obj.get("trip_start_timestamp") != null ){
					tripst = obj.get("trip_start_timestamp").getAsString(); }

				String tripet = "Unknown";
				if ( obj.get("trip_end_timestamp") != null ){
					tripet = obj.get("trip_end_timestamp").getAsString(); }

				int tripseconds = -1;
				if ( obj.get("trip_seconds") != null ){
					tripseconds = obj.get("trip_seconds").getAsInt(); }

				double tripmiles = -1;
				if ( obj.get("trip_miles") != null ){
					tripmiles = obj.get("trip_miles").getAsDouble(); }

				double triptotal = -1;
				if ( obj.get("trip_total") != null ){ 
					triptotal = obj.get("trip_total").getAsDouble(); }
				
				double pickupLat = -1;
				if ( obj.get("pickup_centroid_latitude") != null ){ 
					pickupLat = obj.get("pickup_centroid_latitude").getAsDouble(); }
				
				double pickupLon = -1;
				if ( obj.get("pickup_centroid_longitude") != null ){ 
					pickupLon = obj.get("pickup_centroid_longitude").getAsDouble(); }
				
				double dropoffLat = -1;
				if ( obj.get("dropoff_centroid_latitude") != null ){ 
					dropoffLat = obj.get("dropoff_centroid_latitude").getAsDouble(); }
				
				double dropoffLon = -1;
				if ( obj.get("dropoff_centroid_longitude") != null ){ 
					dropoffLon = obj.get("dropoff_centroid_longitude").getAsDouble(); }
				
				double tolls = 0;
				if ( obj.get("tolls") != null ){ 
					tolls = obj.get("tolls").getAsDouble(); }

				Service nuevo = new Service(id, comp, dca, pca, tripid, tripst, tripet, tripseconds, tripmiles, triptotal,dropoffLat,dropoffLon,pickupLat,pickupLon,tolls);
				if(zonaCorrespondientePickup(nuevo)==null){
					Zona nuevaI = new Zona(nuevo.getPickupLat(), nuevo.getPickupLong(), pDx);
					nuevaI.agregarServicioPickup(nuevo);
					grafo.addVertex(nuevaI.darReferencia(), nuevaI);
					if(zonaCorrespondienteDropoff(nuevo)==null){
						Zona nuevaD = new Zona(nuevo.getDropoffLat(), nuevo.getDropoffLong(), pDx);
						nuevaD.agregarServicioDropoff(nuevo);
						grafo.addVertex(nuevaD.darReferencia(), nuevaD);
					}
					else{
						zonaCorrespondienteDropoff(nuevo).agregarServicioDropoff(nuevo);
					}
				}
				else{
					zonaCorrespondientePickup(nuevo).agregarServicioPickup(nuevo);
					if(zonaCorrespondienteDropoff(nuevo)==null){
						Zona nuevaD = new Zona(nuevo.getDropoffLat(), nuevo.getDropoffLong(), pDx);
						nuevaD.agregarServicioDropoff(nuevo);
						grafo.addVertex(nuevaD.darReferencia(), nuevaD);
					}
					else{
						zonaCorrespondienteDropoff(nuevo).agregarServicioDropoff(nuevo);
					}
				}
				conectarPorServicio(nuevo);
			}
			actualizarPesoArcos();
			System.out.println("\nSe cargaron "+grafo.V()+" vertices.");
			System.out.println("Se cargaron "+grafo.E()+" arcos.");
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();}
	}

	public double toRad(double pAngle){
		return (Math.PI/180)*pAngle;
	}
	public double distanciaHarvesiana(double lat1, double lon1, double lat2, double lon2){
		final int R = 6371*1000;
		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(toRad(lat1))* Math.cos(toRad(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c; //en metros
		double resultado = distance/1609.34; // en millas
		return resultado;
	}
	
	public Zona zonaCorrespondientePickup(Service pServicio){
		NodoKV<Coordenadas,Vertex<Coordenadas,Zona, Double>> nActual = grafo.darListaAdyacencia().darPrimero();
		Zona correspondiente = null;
		while(nActual != null){
			Zona zActual = nActual.getValue().getValue();
			if(zActual.pickupDentroDelRango(pServicio)){
				correspondiente = zActual;
			}
			nActual = nActual.darSiguiente();
		}
		return correspondiente;
	}
	
	public Zona zonaCorrespondienteDropoff(Service pServicio){
		NodoKV<Coordenadas,Vertex<Coordenadas,Zona, Double>> nActual = grafo.darListaAdyacencia().darPrimero();
		Zona correspondiente = null;
		while(nActual != null){
			Zona zActual = nActual.getValue().getValue();
			if(zActual.dropoffDentroDelRango(pServicio)){
				correspondiente = zActual;
			}
			nActual = nActual.darSiguiente();
		}
		return correspondiente;
	}
	
	public void conectarPorServicio(Service pServicio){
		Zona pickup = zonaCorrespondientePickup(pServicio);
		Zona dropoff = zonaCorrespondienteDropoff(pServicio);
		if(pickup != null && dropoff != null && pickup != dropoff){
			Vertex<Coordenadas,Zona,Double> pOrigen = new Vertex<Coordenadas, Zona, Double>(pickup.darReferencia(), pickup);
			Vertex<Coordenadas,Zona,Double> pDestino = new Vertex<Coordenadas, Zona, Double>(dropoff.darReferencia(), pickup);
			grafo.addEdge(pOrigen.getKey(), pDestino.getKey(), new Double(0));
		}
	}
	
	public void actualizarPesoArcos(){
		List<Edge<Coordenadas,Zona,Double>> arcos = grafo.darArcos();
		Nodo<Edge<Coordenadas,Zona,Double>> actual = arcos.darPrimero();
		while(actual != null){
			Edge<Coordenadas,Zona,Double> aActual = actual.darElemento();
			double pesoActual = aActual.getOrigen().getValue().aportePesoPickup();
			Double nuevoPeso = Double.valueOf(pesoActual);
			aActual.cambiarPeso(nuevoPeso);
			actual = actual.darSiguiente();
		}
	}
}